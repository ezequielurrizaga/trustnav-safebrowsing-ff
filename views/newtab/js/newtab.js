/******
	SOLO PARA LEGACY !
*********/
var findGetParameter = function(parameterName) {
    var result = null,
        tmp = [];
    location.search.substr(1).split("&").forEach(function(item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = tmp[1];
    });
    return result;
};
if (!findGetParameter('r')) {
	window.location.href= "https://safesearch.trustnav.com/?src=ext&action=sl";
}

function closeModal() {
	$(".modal").css("opacity","0");
	$(".modal-wrap").fadeOut();
	$("#add-site-form").trigger("reset");
	$(".error").removeClass("error");
	$("#error").hide();
	$("#preview-title").text("Title");
	$("#preview-icon").attr("src","");
	$("#preview-thumbnail").attr("src","");
}

chrome.runtime.sendMessage({ action: 'getSearchEngine' }, function(response) {
	if (response.searchEngine)
		searchEngine = response.searchEngine;
	else
		searchEngine = Configserver.newTabUrls[0];

	/* Obtener traducciones */
	getTranslations(function(translations) {
    	$('#search-box').attr('placeholder',  translations['newtabSearch'] + ' ' + searchEngine.name);
	});
})
var searchEngine;

$(document).ready(function() {
	$('#search-box').focus();

	var URL_REGEXP = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/
	var items = [];

    var search = function() {
        var searchParam = $('#search-box').val();

        if (!searchParam)
            return;

        chrome.runtime.sendMessage({ action: 'newSearch' }, function(response) {});

        var url = searchEngine.url + searchEngine.path;
        var searchUrl = url.replace(/{param}/gi, encodeURIComponent(searchParam));
        top.location.href = searchUrl;
    };

	chrome.runtime.sendMessage({ action: 'getNewTabItem' }, function(response) {
		var i = 0;
		items = response.items;

		if (items.length == 8) {
			$("#add-item").hide();
		}

		for (var item of items) {
			var htmlItem = '<a href="'+item.url+'" target="_parent" class="link-card" style="text-decoration: none;" item-id="'+i+'">' +
								'<div class="item">' +
									'<div class="header">' +
										'<div class="">' +
											'<img height="16px" width="16px" src="https://s2.googleusercontent.com/s2/favicons?domain_url='+item.url+'" alt="">' +
										'</div>' +
										'<div class="url-title">' +
											item.title +
										'</div>' +
										'<div class="close-wrap">' +
											'<span class="close hairline"></span>' +
										'</div>' +
									'</div>' +
									'<div style="display: flex; align-items: center; justify-content: center">' +
										'<img src="'+item.imgLink+'" class="thumbnail" alt="">' +
									'</div>' +
								'</div>'+
							'</a>';

			$(".grid").append(htmlItem);
			i++;
		}
	});

    $('#search-box').keypress(function(event) {
        if (event.which == 13)
            search();
    });

    $('#search-button').click(function() {
        search();
    });

	$('#add-item').click(function() {
		$(".modal-wrap").show();
		$(".modal").css("opacity","1");
	});

	/* Click en item */
	$(document).on("click",".go-url",function(event) {
		/* Obtengo url*/
		var url = $(this).attr("link");
		/* Re-dirigo */
		window.location.href = url;
	});

	/* Desenfoque de input de titulo */
	$(document).on("blur","#title",function() {
		/* Obtengo titulo*/
		var title = $("#title").val();

		/* Si existe */
		if (title) {
			/* Lo pongo en la tarjeta */
			$("#preview-title").text(title);
			/* No existen errores */
			$(this).removeClass("error");
		} else {
			/* Existe error */
			$(this).addClass("error");
		}
	});

	/* Desenfoque de input de thumbnail */
	$(document).on("blur","#thumbnail",function() {
		/* Obtengo src del thumbnail */
		var src = $("#thumbnail").val();
		/* Si existe y cumple con el regex */
		if (src && URL_REGEXP.test(src)) {
			/* Set como thumbnail */
			$("#preview-thumbnail").attr("src",src);
			/* No existen errores */
			$(this).removeClass("error");
		} else {
			/* Existe error */
			$(this).addClass("error");
		}
	});

	/* Desenfoque de input de url */
	$(document).on("blur","#url",function() {
		/* Obtengo src del url */
		var val =  $("#url").val();
		/* Si existe y cumple con el regex */
		if (val && URL_REGEXP.test(val)) {
			/* Armo objeto de url */
			var url = new URL("http://" + val);
			/* Obtengo el dominio */
			var domain = url.host;
			/* Parse de url de favicon */
			var faviconUrl = "https://s2.googleusercontent.com/s2/favicons?domain_url="+domain+"/";
			/* Set src */
			$("#preview-icon").attr("src",faviconUrl);
			/* No existen errores */
			$(this).removeClass("error");
		} else {
			/* Existen errores */
			$(this).addClass("error");
		}
	});



	/* Click en añadir sitio */
	$(document).on("click","#add-site",function(event) {
		/* Flag para saber si existen errores */
		var error = false;
		/* Objeto con informacion de url */
		var data = {
			title : $("#title").val(),
			url : $("#url").val(),
			imgLink : $("#thumbnail").val()
		}

		/* Si la url no es valida */
		if (!data.url || !URL_REGEXP.test(data.url)) {
			/* Error */
			$("#url").addClass("error");
			$("#error").show();
			error = true;
		} else {
			/* No existe error */
			$("#url").removeClass("error");
		}

		/* Si el thumbnail no es valido */
		if (!data.imgLink || !URL_REGEXP.test(data.imgLink)) {
			/* Error */
			$("#thumbnail").addClass("error");
			$("#error").show();
			error = true;
		} else {
			/* No existe error */
			$("#thumbnail").removeClass("error");
		}

		/* Si el titulo no es valido */
		if (!data.title) {
			/* Error */
			$("#title").addClass("error");
			$("#error").show();
			error = true;
		} else {
			/* No existe error */
			$("#title").removeClass("error");
		}

		if (error) {
			return;
		}

		/* Enviar data y cerrar modal */
		chrome.runtime.sendMessage({ action: 'setNewTabItem', item : [data] }, function(response) {
			if ($(".grid > a").length == 7) {
				$("#add-item").hide();
			}

			items.push(data);
			$("#previewItem").attr("item-id", items.length - 1);
			$("#previewItem").attr("href", data.url);

			var newItem = $( "#previewItem" ).clone()
			$(newItem).attr("id", "");
			$(newItem).children().removeAttr( 'style' );
			$(newItem).appendTo( ".grid" );

			$(".modal").css("opacity","0");
			$(".modal-wrap").fadeOut();
			$("#add-site-form").trigger("reset");
			$(".error").removeClass("error");
			$("#error").hide();
			$("#preview-title").text("Title");
			$("#preview-icon").attr("src","");
			$("#preview-thumbnail").attr("src","");
		});

	});

	$(document).on("click","#close-modal",function(event) {
		closeModal();
	});

	$(document).on("click",".close-wrap",function(event) {
		event.preventDefault();
		event.stopPropagation();

		var toDelete = $(this).parents()[2];
		var toDeleteId = $(toDelete).attr("item-id");
		var allNext = $(toDelete).nextAll();
		var that = this;

		chrome.runtime.sendMessage({ action: 'deleteNewTabItem', index : toDeleteId }, function(response) {
			$($(that).parents()[1]).css("opacity",0);
			$(toDelete).css("width","0%");

			for (nextElement of allNext) {
				var index =	$(nextElement).attr("item-id");
				if (index) {
					$(nextElement).attr("item-id", index - 1);
				}
			}

			$(toDelete).on('transitionend', function(e){
			    $(e.target).remove();
				/* Timeout para efecto css de transicion y no generar scroll*/
				setTimeout(function () {
					$("#add-item").show();
				}, 400);
			});

		});
	});

	$(document).on("click",".modal-wrap",function(event) {
		if ($(event.toElement).hasClass("modal-wrap")) {
			closeModal();
		}
	});
});
