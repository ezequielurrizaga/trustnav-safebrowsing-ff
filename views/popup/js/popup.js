/* Variables para inicializar */
var rating = {
    average: null,
    user: null
};
var blockedUrl = false;
var trustnavStatus = true;

/* Init */


var setRatingUI = function() {
    // si trustnav esta activado, controlo rating
    if (trustnavStatus) {
        // oculto todo
        $('#unknownSite').hide();
        $('#safeSite').hide();
        $('#unsafeSite').hide();

        // si la url es bloqueada (sitios localhost, chrome, etc), muestro que esta todo ok y oculto rating
        if (blockedUrl) {
            $('#safeSite').show();
            $('#safebrowsingStatus').show();
            $('#vote').hide();
            $('#voteText').hide();
            $('#disabled-1').hide();
            $('#disabled-2').hide();
            // to do -> ocultar seccion rating
        }
        // si la url no es bloqueada, proceso rating
        else {
            // si llega null es porque se cayo la API. oculto seccion ratings y muestro que está todo ok
            if (rating.average === null) {
                $('#safeSite').show();
                $('#vote').hide();
                $('#voteText').hide();
            }
            // si el rating es 0, es porque todavia no hay info. pongo en unknown
            else if (rating.average == 0) {
                $('#unknownSite').show();
                $('#vote').show();
                $('#voteText').show();
            }
            // si el rating es menor a 40, pongo como unsafe
            else if (rating.average <= 40) {
                $('#unsafeSite').show();
                $('#vote').show();
                $('#voteText').show();
            }
            // el rating es bueno
            else {
                $('#safeSite').show();
                $('#vote').show();
                $('#voteText').show();
            }

            $('#unvoted').hide();
            $('#safeVoted').hide();
            $('#unsafeVoted').hide();
            $('#voteText').hide();
            $('#alreadyVotedText').hide();

            // si el usuario voto negativo
            if (rating.user === 0) {
                $('#alreadyVotedText').show();
                $('#unsafeVoted').show();
                $('#userVote').html(translateKey('mainUnsafe'));
            }
            // si el usuario voto positivo
            else if (rating.user === 1) {
                $('#alreadyVotedText').show();
                $('#safeVoted').show();
                $('#userVote').html(translateKey('mainSafe'));
            }
            // el usuario no voto todavia Y la api no esta caida
            else if (rating.average !== null) {
                $('#voteText').show();
                $('#unvoted').show();
            }

            $('#disabled-1').hide();
            $('#disabled-2').hide();
            $('#safebrowsingStatus').show();
        }
    }
    // si trustnav esta desactivado, muestro switch
    else {
        $('#disabled-1').show();
        $('#disabled-2').show();
        $('#safebrowsingStatus').hide();
        $('#voteText').hide();
        $('#alreadyVotedText').hide();
        $('#vote').hide();
    }
    $('#loader').hide();
}
var getStatus = function() {
    // Lanzo las dos cosas en paralelo y cuando terminen las dos, actualiza la vista
    var count = 0;
    var ready = function() {
        count++;
        if (count == 2) {
            setRatingUI();
        }
    }

    // consulto el estado de las notificaciones
    chrome.runtime.sendMessage({ action: 'getNotificationStatus' }, function(response) {
        if (response && response.status)
            trustnavStatus = true;
        else
            trustnavStatus = false;
        ready();
    })

    // consulto el rating
    chrome.tabs.query({active: true, 'lastFocusedWindow': true}, function(tabs) {
        var tab = tabs[0];
        var domain = parseDomain(tab.url);
        var validUrl = validateUrl(tab.url);

        if (!validUrl)
            blockedUrl = true;

        /* Si es una URL valida, pido el rating */
        if (validUrl) {
            chrome.runtime.sendMessage({
                'action': 'getRating',
                'domain': domain,
                'tab': tab
            }, function(response) {
                if (response && response.rating)
                    rating = response.rating;
                ready();
            });
        }
        /* Si es una URL bloqueada, muestro que está todo OK */
        else {
            ready();
        }
    })
}

var vote = function(safe) {
    // actualizo el UI
    rating.user = safe;
    setRatingUI();

    // envio el rating
    chrome.tabs.query({active: true, 'lastFocusedWindow': true}, function(tabs) {
        var tab = tabs[0];
        var domain = parseDomain(tab.url);
        chrome.runtime.sendMessage({
            action: 'sendRating',
            domain: domain,
            safe: safe
        }, function(response) {});
    });
}

var enableTrustnav = function() {
    chrome.runtime.sendMessage({
        action: 'setNotificationStatus',
        status: false,
        rating: rating
    }, function(response) {});
    trustnavStatus = true;
    setRatingUI();
}

var init = function() {
    getStatus();
}

$(document).ready(function() {
    init();

    $('#voteSafe').click(function() {
        vote(1);
    })
    $('#voteUnsafe').click(function() {
        vote(0);
    })
    $('.undue-vote').click(function() {
        vote(null);
    })
    $('#enableButton').click(function() {
        enableTrustnav();
        changeSwitch("#showMeAlerts", true);
    })
})