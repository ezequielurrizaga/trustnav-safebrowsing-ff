var changeSwitch = function(id, status){
        if (status){
            $(id).removeClass('off');
            $(id).addClass('on');
        }
        else{
            $(id).removeClass('on');
            $(id).addClass('off');
        }
    }

chrome.runtime.sendMessage({action: 'getNotificationStatus'}, function(response) {
    //$("#enableNotifications").prop('checked', response && response.status ? true : false);
    if (response && response.status){
        changeSwitch("#showMeAlerts", true);
    }
    else{
        changeSwitch("#showMeAlerts", false);
    }
})
$(document).on("click", "#showMeAlerts", function() {
    trustnavStatus = !trustnavStatus;
    changeSwitch("#showMeAlerts", trustnavStatus);
    chrome.runtime.sendMessage({action: 'setNotificationStatus', status: !trustnavStatus, rating: rating}, function(response){
        setRatingUI();

    });
});
//$(document).ready(function() {






    chrome.runtime.sendMessage({ action: 'getDisabledNotification' }, function(response) {
        if (response && response.domains) {

            for (var domain of response.domains) {
                $("#setAlertSection .options-group").append('<div class="website-option option-wrap on" value="' + domain + '"><div><span class="option-name">' + domain + '</span></div><div class="option-switch"><div class="track"></div><div class="knob"></div></div></div>');
                //$("#noNotificationSites").append("<option value=" + domain + ">" + domain + "</option>");
            }
            if (!response.domains.length){
                $("#setAlertSection .options-group").append('<div class="option-wrap no-options"><div><span>(Ninguno detectado)</span></div></div>')
            }
            $("#setAlertSection .website-option").on('click', function(){
                clearSiteNotification($(this), $(this).attr('value'));
            });
        }
    });


    /* Pido el new tab url actual */
    chrome.runtime.sendMessage({ action: 'getSearchEngine' }, function(response) {
        var searchEngine = response.searchEngine;
        var selected = null;
        /* Inserto todos los newtaburl que haya en el config */
        for (var i in Configserver.searchEngines) {
            var option = Configserver.searchEngines[i];
            if (searchEngine.url === option.url) {
                selected = i;
                $("#setSearchEngineSection .options-group").append('<div class="option-wrap radio-selected" value="' + i + '"><div><span class="option-name">' + option.name + '</div><div class="option-radio"><div class="radio-circle"><div class="radio-fill"></div></div></div></div>');
            }
            else
                $("#setSearchEngineSection .options-group").append('<div class="option-wrap" value="' + i + '"><div><span class="option-name">' + option.name + '</div><div class="option-radio"><div class="radio-circle"></div></div></div>');
            if (option.recommended){
                $('<span> ' + translateKey('settingsRecommended') + ' </span>').insertAfter($("#setSearchEngineSection .options-group .option-name").last());
            }
        }

        $("#setSearchEngineSection .option-wrap").on('click', function(){
            $("#setSearchEngineSection .option-wrap").each(function(){
                engineSelect($(this), false);
            });
            engineSelect($(this), true);
            var id = $(this).attr('value');
            if (id || id === 0){
                chrome.runtime.sendMessage({ action: 'setSearchEngine', searchEngine: Configserver.searchEngines[id] }, function(response) {});
                console.log('toptions-newtab-url-saved');
            }

        });
    });



    var clearSiteNotification = function(option, value) {
        chrome.runtime.sendMessage({
            action: 'setNotificationStatus',
            domains: [value],
            status: false
        }, function(response) {
            option.remove();
        });
    };


    var settingsOpen = false;
    $(".settingsMenuBtn").on('click', function(){
        if (!settingsOpen){
            settingsOpen = true;
        }
        else
            settingsOpen = false;
        settingsMenu(settingsOpen);

    });

    var sectionAlert = function(){
        $("#setAlert").addClass('selected');
        $("#setAlertSection").show();
        $("#setSearchEngine").removeClass('selected');
        $("#setSearchEngineSection").hide();
        $("#setAbout").removeClass('selected');
        $("#setAboutSection").hide();

    };

    $("#setAlert").on('click', sectionAlert);
    $("#setSearchEngine").on('click', function(){
        $("#setAlert").removeClass('selected');
        $("#setAlertSection").hide();
        $("#setSearchEngine").addClass('selected');
        $("#setSearchEngineSection").show();
        $("#setAbout").removeClass('selected');
        $("#setAboutSection").hide();
    });
    $("#setAbout").on('click', function(){
        $("#setAlert").removeClass('selected');
        $("#setAlertSection").hide();
        $("#setSearchEngine").removeClass('selected');
        $("#setSearchEngineSection").hide();
        $("#setAbout").addClass('selected');
        $("#setAboutSection").show();
    });

    var engineSelect = function(option, value){
        if (!value){
            option.removeClass('radio-selected');
            option.find('.radio-circle').html('');
        }
        else{
            option.addClass('radio-selected');
            option.find('.radio-circle').html('<div class="radio-fill"></div>');
        }
    }

    var settingsMenu = function(value){
        if (value){
            sectionAlert();
            $("#settingsSection").show();
            $("#mainSection").hide();
        }
        else{
            $("#settingsSection").hide();
            $("#mainSection").show();
        }
    }

//});
