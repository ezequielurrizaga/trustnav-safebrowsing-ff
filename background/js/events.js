'use strict';

/***************
    LISTENERS
****************/
var icons = {
    safe: {
        enabled: "/icons/Safe64.png",
        disabled: "/icons/Safe-Disable64.png"
    },
    unsafe: {
        enabled: "/icons/Unsafe64.png",
        disabled: "/icons/Unsafe-Disable64.png"
    },
    unknown: {
        enabled: "/icons/Unknown64.png",
        disabled: "/icons/Unknown-Disable64.png"
    }
}
var lastIcon;

/* Evento de instalacion */
chrome.runtime.onInstalled.addListener(function(details) {
    /* Verifico que sea instalacion */
    if (details.reason == "install") {
        /* Creo un tab para mostrar la landing de instalacion */
        //chrome.tabs.create({ url: Configserver.website + '/install/thankyou.php' });
    }
    /* Verifico si es actualizacion de la extension */
    else if (details.reason == 'update') {
        //chrome.tabs.create({ url: Configserver.website + '/update/' });
        handler.setSearchEngine({ searchEngine: Configserver.searchEngines[0] }, null, function() {});
    }
})

/* Evento de actualizacion disponible */
chrome.runtime.onUpdateAvailable.addListener(function() {
    /* Hago un reload para que se actualice la ext en el momento, y no esperar al proximo restart de chrome */
    chrome.runtime.reload();
});

/* Cambio de seleccion de tab */
chrome.tabs.onActivated.addListener(function() {
    chrome.tabs.query({ 'active': true, 'lastFocusedWindow': true }, function(tabs) {
        if (tabs && tabs[0]) {
            var domain = parseDomain(tabs[0].url);
            var rating = null;
            if (tabStates[tabs[0].id] && tabStates[tabs[0].id].rating)
                rating = tabStates[tabs[0].id].rating;
            updateTab(tabs[0].id, tabs[0].url, tabs[0]);
        }
    })
});

/* Tab creada */
chrome.tabs.onCreated.addListener(function(tab) {
    tabStates[tab.id] = {};

    /* Seteo el icono verde al tab */
    chrome.storage.sync.get('disabledNotification', function(data) {
        // CAMBIO FF
        if (!data)
            data = {}
        // FIN CAMBIO FF
        /* Las notificaciones estan activadas */
        var icon;
        if (data.disabledNotification){
            icon = {
                path: icons.safe.enabled
            }
        }
        else{
            icon = {
                path: icons.safe.disabled
            }
        }
            
        chrome.browserAction.setIcon(icon);
    });

        
});

/* Tab reemplazada */
chrome.tabs.onReplaced.addListener(function(newId, removedId) {
    tabStates[newId] = {};

    /* Guardo en la tab nueva, el ultimo dominio de la tab vieja. Y luego borro la tab vieja */
    if (tabStates[removedId]) {
        if (tabStates[removedId].actualDomain) {
            tabStates[newId].actualDomain = tabStates[removedId].actualDomain;
            tabStates[newId].start = tabStates[removedId].start;
        }
        delete tabStates[removedId];
    }

    /* Obtengo el tab por ID y lo actualizo */
    chrome.tabs.get(newId, function(tab) {
        updateTab(newId, tab.url, tab);
    })
});

/**
 * @event onCloseTab()
 * @desc Evento que se ejecuta cada vez que es cerrada una tab.
 *     Se generara el fin del timeStamp y enviara el evento de cierre de pestaña.
 *
 */
chrome.tabs.onRemoved.addListener(function(tabId) {
    if (tabStates[tabId]) {
        var tabState = tabStates[tabId];
        /* Estaba en un dominio, envio el check domain */
        if (tabState.actualDomain && tabState.start) {
            var elapsedTime = ((new Date()).getTime() - tabState.start);
            elapsedTime = msToTime(elapsedTime);
            // console.log("Estuvo en dominio " + tabState.actualDomain + " por " + elapsedTime);
        }
        delete tabStates[tabId];
    }
});


/**
 * @event onUpdateTab()
 * @desc Evento que se ejecuta cada vez que se actualiza la pestaña (cambios de url).
 *     Se guardara la url que es visitada, y se hara un timeStamp para saber cuanto tiempo paso el usuario en cada dominio
 *
 */

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if (changeInfo.url || changeInfo.status === 'loading') {
        updateTab(tabId, changeInfo.url ? changeInfo.url : tab.url, tab);
    };
});


var updateTab = function(tabId, url, tab) {

    if (!tabStates[tabId])
        tabStates[tabId] = {};
    var tabState = tabStates[tabId];

    var domain = parseDomain(url);
    setBadget(domain, tabId);

    /* Si no cambio el dominio, no tengo que hacer nada */
    if (tabStates[tabId].actualDomain && tabStates[tabId].actualDomain === domain) {
        // console.log("No cambio el dominio.. no pido rating");
        setIconNew(tabStates[tabId].rating, tabId);
        return;
    }
    /* Cambio el dominio o no tenia dominio anterior. Pido rating y actualizo estado del tab */
    else {
        handler.getRating({ tab: tab }, null, function(response) {
            var rating = response.rating;
            var validUrl = validateUrl(url);

            setIconNew(rating, tabId);

            /* Si la URL es valida, verifico el cambio de dominio */
            if (validUrl) {
                if (!tabState.actualDomain || tabState.actualDomain !== domain) {
                    // console.log("Se detecta cambio de dominio.");

                    /* Si tenia dominio previo, cambio el dominio */
                    if (tabState.actualDomain) {
                        tabState.lastDomain = tabState.actualDomain;
                        var elapsedTime = ((new Date()).getTime() - tabState.start);
                        elapsedTime = msToTime(elapsedTime);
                        // console.log("Estuvo en dominio " + tabState.lastDomain + " por " + elapsedTime);
                    }
                }
                tabState.start = new Date();
                tabState.actualDomain = domain;
                // console.log("Actual domain: " + tabState.actualDomain)
                // console.log("Last domain: " + tabState.lastDomain)
            }
            /* Si la URL no es valida */
            else {
                /* Si antes estaba en un dominio, quiere decir que se fue del dominio */
                if (tabState.actualDomain) {
                    // console.log("Se fue a una URL invalida. Fin de dominio");
                    delete tabState.actualDomain;
                    delete tabState.lastDomain;
                }
            }
        });
    }
}

var updateIcon = function(active, rating){
    var icon;
    if (rating && rating.average !== null && rating.average !== undefined) {
        if (rating.average === 0) {
            if (!active){
                icon = {
                    path: icons.unknown.enabled
                }
            }
            else{
                icon = {
                    path: icons.unknown.disabled
                }
                
            }
                
        } else if (rating.average <= 40) {
            if (!active){
                icon = {
                    path: icons.unsafe.enabled
                }
            }
            else{
                icon = {
                    path: icons.unsafe.disabled
                }
            }
        } else {
            if (!active){
                icon = {
                    path: icons.safe.enabled
                }
            }
            else{
                icon = {
                    path: icons.safe.disabled
                }
                
            }
                
        }
    }
    /* Si hubo error lo marco como verde */
    else {
        var icon = {path : null};
        if (!active){
            switch (lastIcon){
                case icons.unknown.disabled: icon.path = icons.unknown.enabled; break;
                case icons.unsafe.disabled: icon.path = icons.unsafe.enabled; break;
                default: icon.path = icons.safe.enabled;
            }
        }
        else{
            switch (lastIcon){
                case icons.unknown.enabled: icon.path = icons.unknown.disabled; break;
                case icons.unsafe.enabled: icon.path = icons.unsafe.disabled; break;
                default: icon.path = icons.safe.disabled;
            }
        }
            
    }
    lastIcon = icon.path;
    chrome.browserAction.setIcon(icon);
}
var setIconNew = function(rating, tabId) {
    chrome.tabs.query({active: true, 'lastFocusedWindow': true}, function(tabs) {
        var tab = tabs[0];
        /* Si la tab activa es igual a la que estamos actualizando, le cambio el icono */
        if (tab.id == tabId) {
            /* Si tiene rating */
                    
            chrome.storage.sync.get('disabledNotification', function(data) {
                // CAMBIO FF
                if (!data)
                    data = {}
                // FIN CAMBIO FF
                /* Las notificaciones estan activadas */
                updateIcon(data.disabledNotification, rating);
            });
        }

    });
};

var setBadget = function(domain, tabId) {
    // handler.getBlockerStatus({ domain: domain }, null, function(status) {
    //     /* Si no tiene domain, analizo solo el general */
    //     if (!status.general || (!status.domain && domain)) {
    //         chrome.browserAction.setBadgeText({
    //             text: '!',
    //             tabId: tabId
    //         });
    //         chrome.browserAction.setBadgeBackgroundColor({
    //             color: '#646464',
    //             tabId: tabId
    //         });
    //     }
    // });
};