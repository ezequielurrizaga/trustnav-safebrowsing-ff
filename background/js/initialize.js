(function() {
    'use strict';

    var register = function() {
        console.log(new Date() + ": Register user");
        $.ajax({
                method: "POST",
                url: Configserver.server + "client",
                headers: {
                    'Authorization': 'Basic 7f1c64fad8a835f867743c9762abc4dccf82832c',
                    'Content-Type': 'application/json'
                }
            })
            .done(function(msg) {
                if (msg && msg.data && msg.data.hash && msg.data.hash !== "") {
                    console.log(new Date() + ": Registered");
                    var token = msg.data.hash;
                    localStorage.setItem('ClientToken_trustnav', token);
                    setUninstallUrl();
                } else {
                    console.log(new Date() + ": Couldn't register. Retrying in 30 seconds");
                    setTimeout(register, 30000);
                }
            })
            .fail(function() {
                console.log(new Date() + ": Couldn't register. Retrying in 30 seconds");
                setTimeout(register, 30000);
            });
    };

    var setUninstallUrl = function() {
        console.log(new Date() + ": Set uninstall url");
        var token = localStorage.getItem('ClientToken_trustnav');
        var url = Configserver.server + "client/uninstall?client=" + token;
        chrome.runtime.setUninstallURL(url, function() {});
    };

    var run = function() {
        console.log(new Date() + ": Run");
        var token = localStorage.getItem('ClientToken_trustnav');
        if (!token)
            register();
        else
            setUninstallUrl();
    };
    run();
})();