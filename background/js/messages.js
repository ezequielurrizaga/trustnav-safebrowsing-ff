'use strict';

const handler = {
    /**
     * Obtener el rating de un dominio. Actualiza el icono de la tab.
     * @param {Object}              message                 El mensaje que contiene el dominio
     *                              -- message.tab          Opcional. Es el objeto del tab al cual evaluar el rating
     * @param {-}                   request                 Viene cuando se realiza la llamada de un content script. Tiene informacion del tab
     * @param {getRatingCallback}   callback                Callback de respuesta
     */

    /**
     * @callback getRatingCallback
     * @param {Object}          Rating
     */
    getRating: function(message, request, callback) {
        /*
         * Trato de obtener el tab que solicita el rating.
         * Necesito el tab para pedir especificamente el rating de la url del tab que lo solicita
         * De lo contrario habria un BUG que al pedir muchas pestañas a la vez (por ejemplo cuando abris el chrome y restauras pestañas),
         * como esta funcion agarraba el tab selected, para todas las pestañas pedia el rating del mismo tab.
         */
        var tab;
        /* Si se llama desde un content script, llega en el request */
        if (request && request.tab)
            tab = request.tab;
        /* Si se hace a mano (popup y events.js), me llega en el objeto del mensaje */
        else if (message.tab)
            tab = message.tab;

        /* Verifico si tiene una tab activa y con url */
        if (!tab || !tab.url) {
            // console.log("No tiene tab activa");
            return callback({ error: "NO_ACTIVE_TAB" });
        }

        if (!tabStates[tab.id])
            tabStates[tab.id] = {};

        var allowedProtocol = validateUrl(tab.url);

        /* Verifico si es una URL valida */
        if (!allowedProtocol) {
            // console.log("Protocolo invalido");
            return callback({ error: "INVALID_URL" });
        }

        var newDomain = parseDomain(tab.url);

        /* Si me pide actualizar forzadamente el rating */
        if (message.force) {
            // console.log("Forzada, pedir rating");
            ajaxGetRating(newDomain, function(error, rating) {
                if (error || !rating) {
                    return callback({ error: "ERROR_OBTAINING_RATING" });
                }

                var clonedRating = $.extend(true, {}, rating); // clono el objeto
                tabStates[tab.id].rating = clonedRating;
                return callback({ rating: clonedRating });
            });
        }
        /* Peticion normal, evaluo si lo tengo que pedir o si ya lo tengo */
        else {
            /* Si ya tengo almacenado el rating, se lo devuelvo */
            if (ratings[newDomain]) {
                // console.log("Entrego rating almacenado");
                var rating = $.extend(true, {}, ratings[newDomain]); // clono el objeto
                tabStates[tab.id].rating = rating;
                return callback({ rating: rating });
            }
            /* Si no esta almacenado, lo tengo que pedir */
            else {
                // console.log("Pido rating de nuevo porque no lo tengo en memoria");
                ajaxGetRating(newDomain, function(error, rating) {
                    if (error || !rating)
                        return callback({ error: "ERROR_OBTAINING_RATING" });

                    var clonedRating = $.extend(true, {}, rating); // clono el objeto
                    tabStates[tab.id].rating = clonedRating;
                    return callback({ rating: clonedRating });
                })
            }
        }
    },

    /**
     * Enviar un rating
     * @param {Object}              message               El mensaje que contiene el rating
     *                              -- message.domain     Dominio
     *                              -- message.safe       Si el dominio es seguro o no. int (1/0)
     * @param {-}                   request               No se utiliza
     * @param {sendRatingCallback}  callback              Callback de respuesta
     */

    /**
     * @callback sendRatingCallback
     * @param {Object}              response
     *                              -- response.error {boolean} - true error / false sin error
     */
    sendRating: function(message, request, callback) {
        var token = localStorage.getItem('ClientToken_trustnav');
        if (token) {
            var dataRating = {
                domain: message.domain,
                safe: message.safe
            };

            $.ajax({
                url: Configserver.server + "rating/new",
                method: 'POST',
                contentType: "application/json",
                headers: {
                    'Authorization': 'Client ' + token
                },
                data: JSON.stringify(dataRating),
                success: function(data) {
                    /* Actualizo el rating local para que quede con la votacion del usuario */
                    if (ratings && ratings[message.domain]) {
                        ratings[message.domain].user = message.safe;
                    }
                    return callback({ error: false });
                },
                error: function(error) {
                    return callback({ error: true });
                }
            });
        } else {
            return callback({ error: true });
        }
    },

    newSearch: function(message, request, callback) {
        var token = localStorage.getItem('ClientToken_trustnav');
        if (token) {
            $.ajax({
                url: Configserver.server + "metrics/countSearch",
                method: 'POST',
                dataType: 'json',
                headers: {
                    'Authorization': 'Client ' + token
                },
                success: function(data) {
                    return callback({ error: false });
                },
                error: function(error) {
                    return callback({ error: true });
                }
            });
        } else {
            return callback({ error: true });
        }

    },

    /**
     * Cambiar el estado de las notificaciones de sitios inseguros
     * @param {Object}              message                 El mensaje que contiene el dominio (si se requiere) y el estado
     *                              -- message.domain       El dominio - solo se envía si se quiere actualizar el estado para el dominio en particular
     *                              -- message.status       El estado - true/false
     * @param {-}                   request                 No se utiliza
     * @param {setNotificationCallback}     callback        Callback de respuesta
     */

    /**
     * @callback setNotificationCallback
     * @param {Boolean}             Siempre llega null, no retorna información
     */
    setNotificationStatus: function(message, request, callback) {
        /* Cambiar el estado para uno en particular */
        if (message.domains && message.domains.length) {
            chrome.storage.sync.get('disabledDomainNotification', function(data) {
                // CAMBIO FF
                if (!data)
                    data = {}
                // FIN CAMBIO FF
                var domains = data.disabledDomainNotification;
                if (!domains)
                    domains = {};

                /* Si me manda false, pide volver a activar las notificaciones */
                if (!message.status) {
                    for (var domain of message.domains)
                        delete domains[domain];

                    chrome.storage.sync.set({ 'disabledDomainNotification': domains }, function() {
                        return callback();
                    });
                }
                /* Me pide bloquear notificaciones para un sitio */
                else {
                    /* Lo bloqueo y guardo */
                    for (var domain of message.domains)
                        domains[domain] = true;
                    chrome.storage.sync.set({ 'disabledDomainNotification': domains }, function() {
                        return callback();
                    });
                }
            });
        }
        /* Cambiar el estado para todos los sitios */
        else {
            chrome.storage.sync.set({ 'disabledNotification': message.status }, function() {
                updateIcon(message.status, message.rating);
                return callback();
            });
        }
    },



    /**
     * Obtener el estado de las notificaciones de sitios inseguros
     * @param {Object}              message                 El mensaje que contiene el dominio (si se requiere)
     *                              -- message.domain       El dominio - solo se envía si se quiere obtener el estado para el dominio en particular
     * @param {-}                   request                 No se utiliza
     * @param {getNotificationCallback}     callback                Callback de respuesta
     */

    /**
     * @callback getNotificationCallback
     * @param {Boolean}             true - notificaciones activadas / false - notificaciones desactivadas
     */
    getNotificationStatus: function(message, request, callback) {
        chrome.storage.sync.get('disabledNotification', function(data) {
            // CAMBIO FF
                if (!data)
                    data = {}
            // FIN CAMBIO FF
            /* Las notificaciones estan activadas */
            if (!data.disabledNotification) {
                /* Si me pidio para un sitio, evaluo si para el sitio estan desactivadas */
                if (message.domain) {
                    chrome.storage.sync.get('disabledDomainNotification', function(dataDomain) {
                        // CAMBIO FF
                        if (!dataDomain)
                            dataDomain = {}
                        // FIN CAMBIO FF
                        /* No hay ningun sitio bloqueado */
                        if (!dataDomain.disabledDomainNotification)
                            return callback({ status: true });
                        /* El sitio no esta bloqueado */
                        else if (!dataDomain.disabledDomainNotification[message.domain])
                            return callback({ status: true });
                        /* El sitio esta bloqueado */
                        else
                            return callback({ status: false });
                    });
                }
                /* No me pidio solo un sitio, respondo que las globales estan activadas */
                else {
                    return callback({ status: true });
                }
            }
            /* Estan desactivadas las notificaciones */
            else {
                return callback({ status: false });
            }
        });
    },

    getDisabledNotification: function(message, request, callback) {
        chrome.storage.sync.get('disabledDomainNotification', function(dataDomain) {
            // CAMBIO FF
            if (!dataDomain)
                dataDomain = {}
            // FIN CAMBIO FF
            var domains = [];
            if (dataDomain.disabledDomainNotification) {
                for (var domain in dataDomain.disabledDomainNotification)
                    domains.push(domain);
            }
            return callback({ domains: domains });
        });
    },

    getSearchEngine: function(message, request, callback) {
        var that = this;

        /* Si ya lo tengo en memoria */
        if (searchEngine) {
            return callback({ searchEngine: searchEngine });
        }

        /* No lo tengo en memoria, lo pido y lo guardo */
        chrome.storage.sync.get('searchEngine', function(response) {
            // CAMBIO FF
            if (!response)
                response = {}
            // FIN CAMBIO FF
            searchEngine = response.searchEngine;

            /* Si todavia no tiene guardado ninguno, guardo el primero y lo devuelvo */
            if (!searchEngine) {
                that.setSearchEngine({ searchEngine: Configserver.searchEngines[0] }, null, function() {
                    return callback({ searchEngine: searchEngine });
                });
            }
            /* Estaba guardado el seleccionado, lo devuelvo */
            else {
                return callback({ searchEngine: searchEngine });
            }
        });
    },

    setSearchEngine: function(message, request, callback) {
        /* Lo guardo en memoria */
        searchEngine = message.searchEngine;
        chrome.storage.sync.set({ searchEngine: searchEngine }, function() {
            return callback();
        });
    },

    setNewTabItem: function(message, request, callback) {
        /* Obtengo los items */
        chrome.storage.sync.get('newTabItems', function(response) {
            if (!response)
                response = {}
            // FIN CAMBIO FF
            var items = response.newTabItems;
            /* Si llega undefined*/
            if (!items) {
                /* Items es un array */
                items = [];
            }
            /*Si ya hay 8 o mas existe un error */
            if (items.length >= 8) {
                return callback();
            }

            /* Itero sobre los items q me llegan para darles un id*/
            for (var i = 0; i < message.item.length; i++) {
                /* Agrego item a mi variable de items*/
                items.push(message.item[i]);
            }

            /*Set de mi variable items, con los items que me trago el sync.get + los que fueron pedidos para agregarse*/
            chrome.storage.sync.set({ 'newTabItems': items }, function() {
                /* Lo guardo en memoria */
                newTabItems = items;
                /* Retorno id insertado */
                return callback();
            });

        });
    },

    deleteNewTabItem: function(message, request, callback) {
        /* Obtengo los items */
        chrome.storage.sync.get('newTabItems', function(response) {
            if (!response)
                response = {}
            // FIN CAMBIO FF
            var items = response.newTabItems;
            /* Borro el item a travez del index */
            items.splice(message.index, 1);
            /*Set de items sin el que fue a eliminar por el user*/
            chrome.storage.sync.set({ 'newTabItems': items }, function() {
                /* Lo guardo en memoria */
                newTabItems = items;
                return callback();
            });
        });

    },

    getNewTabItem: function(message, request, callback) {
        var that = this;

        /* Si ya lo tengo en memoria */
        if (newTabItems) {
            return callback({ items: newTabItems });
        }

        /* No lo tengo en memoria, lo pido y lo guardo */
        chrome.storage.sync.get('newTabItems', function(response) {
            if (!response)
                response = {}
            // FIN CAMBIO FF
            newTabItems = response.newTabItems;

            /* Si no tiene definido ninguno todavia, guardo los por defecto */
            if (!newTabItems) {
                /* Guardo los del config y lo devuelvo */
                that.setNewTabItem({ item: Configserver.suggestedItems }, null, function() {
                    return callback({ items: newTabItems });
                });
            }
            /* Ya tiene definido uno, lo retorno */
            else {
                return callback({ items: newTabItems });
            }

        });
    },
}


/* Escuchar eventos */
chrome.runtime.onMessage.addListener(function(message, sender, callback) {
    if (handler[message.action]) {
        handler[message.action](message, sender, callback);
        return true;
    }
})


/**
 * Obtiene el rating de la API
 * @param {String}              domain                  El dominio a consultar en la API
 * @param {getAPIRatingCallback}  callback                Callback de respuesta
 */

/**
 * @callback getAPIRatingCallback
 * @param {Boolean}           error                     Si hubo error, retorna true. Si no, null.
 * @param {Object}            rating                    Si no hubo error, contiene la información del rating obtenido de la base de datos
 */
var ajaxGetRating = function(domain, callback) {
    var token = localStorage.getItem('ClientToken_trustnav');
    if (token) {
        $.ajax({
            url: Configserver.server + "rating/new",
            method: 'GET',
            dataType: 'json',
            headers: {
                'Authorization': 'Client ' + token,
            },
            data: {
                domain: domain
            },
            success: function(data) {
                if (!data || !data.data)
                    return callback(true);
                ratings[domain] = data.data;
                var expiration = (new Date().getTime()) + 14400000; // 4 horas
                ratings[domain].expires = expiration;
                return callback(null, ratings[domain]);
            },
            error: function() {
                return callback(true);
            }
        });
    }
    /* No tiene token, no hago la peticion */
    else
        return callback(true);
};


/**
 * Actualizar el estado del badge despues de actualizar el estado del bloqueador de anuncios
 * @param {Integer}  status             Estado del bloqueador
 * @param {String}   domain             Dominio en el cual desactivar el bloqueador. (se recibe solo cuando no se trata de cambio de estado global)
 */
var updateBadget = function(status, domain) {
    /* Activar bloqueador */
    if (status) {
        /* Reactivar de manera global */
        if (!domain) {
            /* Tengo que cambiar el badget de todos los tabs */
            chrome.tabs.query({}, function(tabs) {
                for (var tab of tabs)
                    removeBadge(tab);
            });
        }
        /* Reactivar para un dominio. Busco todas las tabs cuyo dominio sea el que estoy activando */
        else {
            chrome.tabs.query({}, function(tabs) {
                for (var tab of tabs) {
                    var tabDomain = parseDomain(tab.url);
                    if (domain === tabDomain)
                        removeBadge(tab);
                }
            });
        }
    }
    /* Solicita desactivar */
    else {
        /* Desactivar globalmente */
        if (!domain) {
            /* Tengo que cambiar el badget de todos los tabs */
            chrome.tabs.query({}, function(tabs) {
                for (var tab of tabs)
                    showBadge(tab);
            });
        }
        /* Desactivar para un dominio. Si la URL actual es valida, muestro el badget en el tab */
        else {
            chrome.tabs.query({active: true, 'lastFocusedWindow': true}, function(tabs) {
                var tab = tabs[0];
                if (validateUrl(tab.url))
                    showBadge(tab);
            });
        }
    }
};


/**
 * Mostrar el badge que indica que el bloqueador está desactivado
 * @param {Object}  tab             El objeto de la tab
 */
var showBadge = function(tab) {
    chrome.browserAction.setBadgeText({
        text: "!",
        tabId: tab.id
    });
    chrome.browserAction.setBadgeBackgroundColor({
        color: "#646464"
    });
}

/**
 * Ocultar el badge que indica que el bloqueador está desactivado
 * @param {Object}  tab             El objeto de la tab
 */
var removeBadge = function(tab) {
    /* Obtengo el dominio del tab en cuestion y me fijo si para ese dominio esta activado el bloqueador.
     * Esto es para prevenir sacar el badget de tabs cuyo dominio esta desactivado, al reactivar de manera global */
    var domain = parseDomain(tab.url);
    // handler.getBlockerStatus({ domain: domain }, null, function(status) {
    //     /* Si esta activado para el dominio de la tab, oculto el badget */
    //     if (status.domain) {
    //         /* Reviso que el badgeText sea "!", esto es para prevenir que adblock ya lo haya pisado con un numero y lo ocultemos */
    //         chrome.browserAction.getBadgeText({
    //             tabId: tab.id
    //         }, function(text) {
    //             if (text == '!') {
    //                 chrome.browserAction.setBadgeText({
    //                     text: "",
    //                     tabId: tab.id
    //                 });
    //             }
    //         })
    //     }
    // });
}
