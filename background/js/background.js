'use strict';

var ratings = {};
var tabStates = {};
var searchEngine;
var newTabItems;

/* Ir removiendo los ratings viejos */
setInterval(function() {
	// console.log("Remover ratings viejos");
	var now = new Date().getTime();
	for (var domain in ratings) {
		if (ratings[domain].expires <= now) {
			// console.log("Dominio " + domain + " rating expirado");
			delete ratings[domain];
		}
	}
}, 60000);
