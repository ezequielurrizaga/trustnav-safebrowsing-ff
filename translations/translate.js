var langs = ['en', 'es'];
var langCode = 'en';
var translations;

var getTranslations = function(callback) {
    langCode = navigator.language.substr(0, 2);

    if (langs.indexOf(langCode) < 0)
        langCode = 'en';

    /* Obtener traducciones */
    $.getJSON('/translations/assets/' + langCode + '.json', function(data) {
        return callback(data);
    });
}

var translate = function() {
    if (translations) {
        $("[tkey]").each(function(index) {
            var t = translations[$(this).attr('tkey')];
            $(this).html(t);
        });
    }
}

var translateKey = function(key) {
    return translations ? translations[key] : "";
}

getTranslations(function(data) {
    translations = data;
    translate();
});