var Configserver = {
	server: "http://server.trustnav.com:3250/",
	website: "https://www.trustnav.com",
	suggestedItems : [{
		title: "Facebook",
		url: "https://www.facebook.com",
		imgLink : "/views/newtab/img/facebook.png",
	},{
		title: "Twitter",
		url: "https://www.twitter.com",
		imgLink : "/views/newtab/img/twitter.png",
	},{
		title: "Youtube",
		url: "https://www.youtube.com",
		imgLink : "/views/newtab/img/youtube.png",
	},{
		title: "Linkedin",
		url: "https://www.linkedin.com",
		imgLink : "/views/newtab/img/linkedin.png",
	},{
		title: "Amazon",
		url: "https://www.amazon.com",
		imgLink : "/views/newtab/img/amazon.png",
	}],
	searchEngines: [{
		name: "Trustnav Safesearch",
		url: "http://www.google.com",
		path: "/search?q={param}",
		icon: "/views/newtab/img/google.png",
		recommended: true
	},{
		name: "Google",
		url: "https://www.google.com",
		path: "/search?q={param}",
		icon: "/views/newtab/img/google.png"
	}, {
		name: "Norton Safe Search",
		url: "https://nortonsafe.search.ask.com",
		path: "/web?q={param}",
		icon: "/views/newtab/img/norton.png"
	}, {
		name: "Bing",
		url: "https://www.bing.com",
		path: "/search?q={param}",
		icon: "/views/newtab/img/bing.png"
	}, {
		name: "Yahoo",
		url: "https://search.yahoo.com",
		path: "/search?p={param}",
		icon: "/views/newtab/img/yahoo.png"
	}, {
		name: "Ask",
		url: "http://www.ask.com",
		path: "/web?q={param}",
		icon: "/views/newtab/img/ask.png"
	}, {
		name: "DuckDuckGo",
		url: "https://duckduckgo.com",
		path: "/?q={param}",
		icon: "/views/newtab/img/duck.png"
	}]
};
