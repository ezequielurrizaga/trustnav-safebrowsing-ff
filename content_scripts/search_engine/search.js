var findGetParameter = function(parameterName) {
    var result = null,
        tmp = [];
    location.search.substr(1).split("&").forEach(function(item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = tmp[1];
    });
    return result;
};

if (findGetParameter('src') === 'ext') {
    let action = findGetParameter('action') || 'search';
    if (action == 'search') {
        chrome.runtime.sendMessage({ action: 'newSearch' }, function(response) {});
        chrome.runtime.sendMessage({ action: 'getSearchEngine' }, function(response) {
            var url = response.searchEngine.url + response.searchEngine.path;
            var param = findGetParameter('q') || "";
            var searchUrl = url.replace(/{param}/gi, param);
            window.location.href = searchUrl;
        });

    } else if (action == 'homepage') {
        chrome.runtime.sendMessage({ action: 'getSearchEngine' }, function(response) {
            window.location.href = response.searchEngine.url;
        });
    } else if (action == 'sl') {
        var iframe = document.createElement('iframe');
        iframe.src = chrome.extension.getURL('/views/newtab/newtab.html')+'?r=1';
        document.body.appendChild(iframe);
    }
}
