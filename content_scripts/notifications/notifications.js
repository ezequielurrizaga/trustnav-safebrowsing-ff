var langs = ['en', 'es'];
var userLang = navigator.language.substr(0, 2);
if (langs.indexOf(userLang) < 0)
    userLang = 'en';

$(document).ready(function() {
    /* Evento para cerrar el iframe si clickea en cualquier parte del sitio */
    $("html").on("click", closeIframe);
    chrome.runtime.sendMessage({ action: 'getRating' }, function(response) {
        /* Si se recibio rating */
        if (response && response.rating) {
            var average = response.rating.average;
            var userRating = response.rating.user;
            /* Si el promedio merece mostrar rating */
            if (average !== null && average !== 0 && average !== undefined && average <= 40 && userRating !== 1) {
                /* Obtengo el estado de las notificaciones */
                chrome.runtime.sendMessage({ action: 'getNotificationStatus', domain: parseDomain(window.location.href) }, function(notification) {
                    /* Si tengo que mostrar la notificacion */
                    if (notification.status) {
                        // Inserto iframe para meter el html
                        
                        $("html").prepend("<iframe name='trustnav_iframe' id='trustnav_iframe' style='z-index: 999999; position: fixed; right: 15px; top: 15px; width: 642px; height: 260px; border-radius: 8px; background-color: #ffffff; box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.3); border: solid 1px #d7d7d7;' src='" + chrome.runtime.getURL('/views/notifications/notifications.html') + "'></iframe>");
                        //Obtengo el template
                        // var html = getFrameHtml('/views/notifications/notifications.html');
                        // //Obtengo el iframe e inserto el HTML
                        // var doc = document.getElementById('trustnav_iframe').contentWindow.document;
                        // console.log('document.getElementById("trustnav_iframe").contentWindow.document.open');
                        // try{
                        //     doc.open();    
                        // }
                        // catch(e){
                        //     console.log(e);
                        // }
                        
                        // doc.write('html');
                        // doc.close();
                        /* Agrego los eventos necesarios */
                        setTimeout(function(){
                            var iframe = $("#trustnav_iframe").contents();

                            // Click al boton para desactivar notificaciones en el sitio
                            iframe.find("#disableNotifications").on('click', function() {
                                chrome.runtime.sendMessage({
                                    'action': 'setNotificationStatus',
                                    'domains': [parseDomain(window.location.href)],
                                    'status': true
                                }, function() {});
                                closeIframe();
                            });
                            // Click al boton para desactivar notifiaciones en el total
                            iframe.find("#disableAllNotifications").on('click', function() {
                                chrome.runtime.sendMessage({
                                    'action': 'setNotificationStatus',
                                    'status': true
                                }, function() {});
                                closeIframe();
                            });
                            // Click en cerrar iframe
                            iframe.find("#continue").on('click', closeIframe);
                            // Click en salir de la pagina
                            iframe.find("#leave").on('click', function() {
                                window.location.href = "https://www.trustnav.com";
                            });

                            // Traducir el iframe
                            $.getJSON(chrome.extension.getURL('translations/assets/' + userLang + '.json'), function(language) {
                                iframe.find("[tkey]").each(function(i) {
                                    var translation = language[$(this).attr('tkey')];
                                    $(this).html(translation);
                                });
                            });
                        }, 200);
                            
                    }
                })
            }
        }
    });
});


var closeIframe = function() {
    if ($("#trustnav_iframe").length != 0)
        $("#trustnav_iframe").remove();
}